import React from 'react'
import { Table,Button } from 'react-bootstrap'


function TableUser(props) {
    
    const temp = props.account.filter(item =>{
        return item.id > 0
    })
    // console.log("Props: "+temp)
    return (
        <div>
            <>
        <h2>Table Accounts</h2>
        <Table >
            <thead>
                <tr>
                    <th>#</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Gender</th>
                </tr>
            </thead>
            <tbody> 
               {temp.map((item,index)=>(
                    <tr key={index} onClick={()=>{props.eventSelect(index)}} 
                    className={item.clickAccount === true ? "bg-dark text-white":""}>
                        <td>{item.id}</td>
                        <td>{item.username}</td>
                        <td>{item.gender}</td>
                        <td>{item.email}</td>
                    </tr>
                ))}    
            </tbody>
        </Table>
        <Button type="submit" variant="danger" onClick={()=>{props.onDelete()}}>Delete</Button>
        </>
        </div>
    )
}

export default TableUser
