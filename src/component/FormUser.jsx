import React, { Component } from 'react'
import { Col, Form, Row, Button } from 'react-bootstrap'


export default class FormUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            usernanme: "",
            email: "",
            gender: "",
            password: "",
            emailErr: "",
            usernameErr: "",
            passwordErr: "",
        }
    }
    onSave = (index) => {
        let temp = [...this.state.account];
        temp[index].id++;
        this.setState({
            account: temp,
        });
    }
    // onGive = ()=>{
    //     return this.state
    // }

    onhandle = (e) => {
        let name = e.target.name;
        this.setState({
            [name]: e.target.value
        }, () => {
            if (e.target.name === "username") {
                if (this.state.usernanme === "") {
                    this.setState({
                        usernameErr: "Username cannot be empty!",
                    });
                } else {
                    this.setState({
                        usernameErr: "",
                    });
                }
            }
            if (e.target.name === "email") {
                let pattern = /^\S+@\S+\.[a-z]{3}$/g;
                let result = pattern.test(this.state.email.trim());

                if (result) {
                    this.setState({
                        emailErr: "",
                    });
                } else if (this.state.email === "") {
                    this.setState({
                        emailErr: "Email cannot be empty!",
                    });
                } else {
                    this.setState({
                        emailErr: "Email is invalid!",
                    });
                }
            }

            if (e.target.name === "password") {
                let pattern2 =
                    /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_\W])([a-zA-Z0-9_\W]{8,})$/g;
                let result2 = pattern2.test(this.state.password);

                if (result2) {
                    this.setState({
                        passwordErr: "",
                    });
                } else if (this.state.password === "") {
                    this.setState({
                        passwordErr: "Password cannot be empty!",
                    });
                } else {
                    this.setState({
                        passwordErr: "Password is invalid!",
                    });
                }
            }
            console.log(this.state);
        })
    }

    render() {
        return (
            <>
            <Form>
                <h2>Create Account</h2>
                <Form.Group controlId="formGroupUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control name="username" onChange={this.onhandle} type="text" placeholder="Username" />
                    <Form.Text className="text-muted">
                        <p style={{ color: "red" }}>{this.state.usernameErr}</p>
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="formGroupGender">
                    <Row>
                        <Col>
                            <Form.Check onChange={this.onhandle} name="gender" value="male" type="radio" aria-label="first radio" id="formHorizontalRadios1" label="Male" />
                        </Col>
                        <Col>
                            <Form.Check onChange={this.onhandle} name="gender" value="female" type="radio" aria-label=" second radio " id="formHorizontalRadios2" label="Female" />
                        </Col>
                    </Row>
                </Form.Group>
                <Form.Group controlId="formGroupEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control name="email" onChange={this.onhandle} type="email" placeholder="Email" />
                    <Form.Text className="text-muted">
                        <p style={{ color: "red" }}>{this.state.emailErr}</p>
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="formGroupPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control name="password" onChange={this.onhandle} type="password" placeholder="Password" />
                    <Form.Text className="text-muted">
                        <p style={{ color: "red" }}>{this.state.passwordErr}</p>
                    </Form.Text>
                </Form.Group>               
            </Form>
            <Button type="submit" onClick={()=>{this.props.getData(this.state)}}>Save</Button>
            </>
        )
    }
}
