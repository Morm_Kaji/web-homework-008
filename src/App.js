import React, { Component } from 'react'
import { Container, Row, Col, Navbar } from 'react-bootstrap'
import FormUser from './component/FormUser'
import TableUser from './component/TableUser'
import NavbarUser from './component/NavBarUser'

export default class App extends Component {

  constructor() {
    super();
    this.state = {
      account: [{
        id: 0,
        username: "",
        gender: "",
        email: "",
        password: "",
        clickAccount: false,
      }],

    }
    this.getData = this.getData.bind(this);
  }
  getData(item) {
    let addData = {
      id: this.state.account.length,
      username: item.username,
      gender: item.gender,
      email: item.email,
      password: item.password,
      clickAccount: false,
    }
    let newData = [...this.state.account, addData]
    this.setState({
      account: newData
    })
  }

  eventSelect = (index) => {
    const temp = this.state.account.filter(item => {
      return item.id > 0
    })
    let item = [...temp]
    item[index].clickAccount = !item[index].clickAccount
    this.setState({
      account: item
    })
  }

  onDelete = (index) => {
    let temp = this.state.account.filter(item=>{
      return item.clickAccount === false
    })
    this.setState({
      account: temp
    })
  }
  render() {
    return (
      <>
        <NavbarUser />
        <Container>
          <Row>
            <Col>
              <FormUser
                getData={this.getData}
              />
            </Col>
            <Col>
              <TableUser
                account={this.state.account}
                eventSelect={this.eventSelect}
              onDelete = {this.onDelete}
              />
            </Col>
          </Row>
        </Container>
      </>
    );

  }


}
